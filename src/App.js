import React from 'react';
import logo from './logo.svg';
import './App.css';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {theme} from '../src/config/theme'
import Main from '../src/pages/main'
function App() {
  return (
    <ThemeProvider theme={theme}>
      <Button variant="contained" color="secondary" href="#contained-buttons">
        Link
      </Button>
      <Main />
    </ThemeProvider>
  );
}

export default App;
