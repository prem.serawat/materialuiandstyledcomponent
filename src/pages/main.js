import React from 'react';
  
  import {
    Title,Wrapper,StyledButton
  } from '../components/button'
function App() {
  return (
<Wrapper>
    <Title >
        Headding Title
    </Title>
    <StyledButton>
        Button Using Styled
    </StyledButton>
</Wrapper>
  )
   
}

export default App;
